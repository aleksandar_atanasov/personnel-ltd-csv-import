<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CallController@index')->name('home');

Route::post('/calls/import', 'ImportController@import')->name('import.call');
Route::get('/call/{call}', 'CallController@show')->name('show.call');
Route::post('/call', 'CallController@store')->name('store.call');
Route::get('/call/{call:id}/edit', 'CallController@edit')->name('edit.call');
Route::put('/call/{call:id}', 'CallController@update')->name('update.call');
Route::delete('/call/{call}', 'CallController@destroy')->name('delete.call');

Route::get('/users', 'UserController@index')->name('index.user');
Route::get('/user/{user}', 'UserController@show')->name('show.user');
