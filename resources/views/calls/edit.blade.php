@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-6">
            <a href="{{route('show.call', $call->id)}}">
                <button class="btn btn-primary btn-sm">Back</button>
            </a>
            <div class="card mt-3">
                <div class="card-header">{{ __('Edit Call') }}</div>

                <div class="card-body">
                <form action="{{route('update.call', $call->id)}}" method="post">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <select class="form-control" id="user" name="user_id">
                                <option selected value="{{$call->user->id}}">{{$call->user->fullname}}</option>
                                @foreach ($users as $user)
                                    <option value="{{$user->id}}">{{$user->fullname}}</option>
                                @endforeach
                            </select>
                            @error('user_id')
                                <div class="alert text-danger">
                                    {{$message}}
                                </div>
                            @enderror
                          </div>
                          <div class="form-group">
                            <select class="form-control" id="client" name="client_id">
                                <option selected value="{{$call->client->id}}">{{$call->client->fullname}}</option>
                                @foreach ($clients as $client)
                                    <option value="{{$client->id}}">{{$client->fullname}}</option>
                                @endforeach
                            </select>
                            @error('client_id')
                                <div class="alert text-danger">
                                    {{$message}}
                                </div>
                            @enderror
                          </div>
                          <div class="form-group">
                            <input type="number" name="duration" class="form-control" placeholder="Enter duration of the call" value="{{ $call->duration }}">
                            @error('duration')
                                <div class="alert text-danger">
                                    {{$message}}
                                </div>
                            @enderror
                          </div>
                          <div class="form-group">
                            <select class="form-control" id="callType" name="type_of_call">
                                <option selected value="{{$call->type_of_call}}">{{$call->type_of_call}}</option>
                                <option value="{{$call->type_of_call == 'Incoming' ? 'Outgoing' : 'Incoming'}}">
                                    {{$call->type_of_call == 'Incoming' ? 'Outgoing' : 'Incoming'}}
                                </option>
                            </select>
                            @error('type_of_call')
                                <div class="alert text-danger">
                                    {{$message}}
                                </div>
                             @enderror
                          </div>
                          <div class="form-group">
                            <input type="number" name="call_score" class="form-control" placeholder="Enter external call score" value="{{ $call->call_score }}">
                            @error('call_score')
                                <div class="alert text-danger">
                                    {{$message}}
                                </div>
                             @enderror
                          </div>
                        <button type="submit" class="btn btn-primary btn-block">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection