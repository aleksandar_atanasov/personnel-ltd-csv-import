<div class="modal fade" id="createModal" tabindex="-1" aria-labelledby="createModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add New Call</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('store.call')}}" method="POST">
                @csrf
                <div class="form-group">
                    <select class="form-control" id="user" name="user_id">
                        <option label="Select User" hidden>Select User</option>
                        @foreach ($users as $user)
                            <option value="{{$user->id}}">{{$user->fullname}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <select class="form-control" id="client" name="client_id">
                        <option label="Select Client" hidden>Select Client</option>
                        @foreach ($clients as $client)
                            <option value="{{$client->id}}">{{$client->fullname}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <input type="number" name="duration" class="form-control" placeholder="Enter duration of the call" value="{{ old('duration') }}">
                  </div>
                  <div class="form-group">
                    <select class="form-control" id="callType" name="type_of_call">
                        <option label="Select Type Of Call" hidden>Select Type Of Call</option>
                        <option value="Incoming">Incoming</option>
                        <option value="Outgoing">Outgoing</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <input type="number" name="call_score" class="form-control" placeholder="Enter external call score" value="{{ old('call_score') }}">
                  </div>
                  <button type="submit" class="btn btn-primary btn-block">Add</button>
            </form>
        </div>
      </div>
    </div>
  </div>