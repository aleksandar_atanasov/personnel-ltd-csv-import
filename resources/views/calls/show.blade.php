@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-10">
            <a href="{{route('home')}}">
                <button class="btn btn-primary btn-sm">Back</button>
            </a>
                <table class="table mt-3">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Client</th>
                            <th>Client Type</th>
                            <th>Date</th>
                            <th>Duration</th>
                            <th>Type Of Call</th>
                            <th>External Call Score</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$call->user->fullname}}</td>
                            <td>{{$call->client->fullname}}</td>
                            <td>{{$call->client->client_type}}</td>
                            <td>{{$call->date}}</td>
                            <td>{{$call->duration}}</td>
                            <td>{{$call->type_of_call}}</td>
                            <td>{{$call->call_score}}</td>
                            <td class="d-flex align-items-center">
                                <a href="{{route('edit.call', $call->id)}}">
                                    <button class="btn btn-secondary btn-sm">Edit</button>
                                </a>&nbsp;
                                <form method="POST" action="{{route('delete.call', $call->id)}}" id="deleteForm">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger btn-sm">
                                            Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>
                @include('alerts.messages')
        </div>
    </div>
</div>
@endsection