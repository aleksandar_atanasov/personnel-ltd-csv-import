@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row justify-content-end mt-3">
        <div class="col-md-3">
            <a href="{{route('index.user')}}">
                <button class="btn btn-secondary">View Users</button>
            </a>
            <button class="btn btn-primary button"
                data-toggle="modal"
                data-target="#createModal">
                Create New Call
            </button>
        </div>
    </div>
</div>
<div class="container" id="mainHeading">
    <div class="row justify-content-center mt-5 mb-5">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    CSV File Import
                </div>
                <div class="card-body">
                    <form method="POST" action="{{route('import.call')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                          <input type="file" class="form-control-file" id="file" name="file">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Import</button>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
   @include('alerts.messages')
</div>
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <div class="d-flex">
                <h5>Registered Calls</h5>
            </div>
                <table class="table" id="callsTable">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>Client</th>
                            <th>Client Type</th>
                            <th>Date</th>
                            <th>Duration</th>
                            <th>Type Of Call</th>
                            <th>External Call Score</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($calls as $call)
                            <tr>
                                <td>{{$call->user->fullname}}</td>
                                <td>{{$call->client->fullname}}</td>
                                <td>{{$call->client->client_type}}</td>
                                <td>{{$call->date}}</td>
                                <td>{{$call->duration}}</td>
                                <td>{{$call->type_of_call}}</td>
                                <td>{{$call->call_score}}</td>
                                <td>
                                    <a href="{{route('show.call', $call->id)}}">
                                        <button class="btn btn-primary btn-sm">View</button>
                                    </a>&nbsp;
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$calls->links()}}
            </div>
        </div>
    </div>
</div>
@include('calls.create')
@endsection

