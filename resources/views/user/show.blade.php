@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-10">
            <a href="{{route('index.user')}}">
                <button class="btn btn-primary btn-sm">Back</button>
            </a>
            <div class="mt-3">
                <h5>User Details</h5>
            </div>
                <table class="table mt-3">
                    <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>Average User Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$user->fullname}}</td>
                            <td>
                               {{$user->averageScore()}}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <h5>Latest calls</h5>
                <table class="table">
                        <thead>
                            <tr>
                                <th>Client</th>
                                <th>Client Type</th>
                                <th>Date</th>
                                <th>Duration</th>
                                <th>Type Of Call</th>
                                <th>External Call Score</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($user->latestCalls() as $call)
                                <tr>
                                    <td>{{$call->client->fullname}}</td>
                                    <td>{{$call->client->client_type}}</td>
                                    <td>{{$call->date}}</td>
                                    <td>{{$call->duration}}</td>
                                    <td>{{$call->type_of_call}}</td>
                                    <td>{{$call->call_score}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
                @include('alerts.messages')
        </div>
    </div>
</div>
@endsection