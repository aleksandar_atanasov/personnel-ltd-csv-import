@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <a href="{{route('home')}}">
                <button class="btn btn-primary btn-sm">Back</button>
            </a>
            <div class="d-flex justify-content-between mt-2">
                <h5>Users</h5>
            </div>
            <table class="table" id="callsTable">
                <thead>
                    <tr>
                        <th>Full Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{$user->fullname}}</td>
                            <td>
                                <a href="{{route('show.user', $user->id)}}">
                                    <button class="btn btn-primary btn-sm">View</button>
                                </a>&nbsp;
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$users->links()}}
        </div>
    </div>
</div>

@endsection