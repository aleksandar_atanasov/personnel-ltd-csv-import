<?php

namespace App\Imports;

use App\Call;
use App\User;
use App\Client;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class CsvImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        foreach($rows as $row)
        {
            $user = User::updateOrCreate([
                'fullname' => $row['user']
            ]);

            $client = Client::updateOrCreate([
                'fullname' => $row['client'],
                'client_type' => $row['client_type']
            ]);

            $user->calls()->create([
                'user_id' => $user->id,
                'client_id' => $client->id,
                'date' => $row['date'],
                'duration' => $row['duration'],
                'type_of_call' => $row['type_of_call'],
                'call_score' => $row['external_call_score']
            ]);
        }
    }

}
