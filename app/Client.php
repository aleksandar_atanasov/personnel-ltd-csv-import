<?php

namespace App;

use App\Call;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['fullname', 'client_type'];

    public function calls()
    {
        return $this->hasMany(Call::class);
    }
}
