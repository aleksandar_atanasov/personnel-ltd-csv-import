<?php

namespace App\Http\Controllers;

use App\Call;
use App\User;
use App\Client;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\CallRequest;


class CallController extends Controller
{
    public function index()
    {
        $calls = Call::latest()->paginate(5);

        $users = User::all();

        $clients = Client::all();

        return view('welcome', compact(['calls','users','clients']));
    }

    public function show(Call $call)
    {
        return view('calls.show', compact('call'));
    }

    public function store(CallRequest $request)
    {
        $data = $request->validated();

        try {
            $call = Call::create($data);

            $call['date'] = Carbon::now();

            return redirect()->back()->withSuccess('Call succesfully added');

        } catch (\Throwable $th) {

            return redirect()->back()->withErrors('Something went wrong! Try again later..');

        }
    }

    public function edit(Call $call)
    {
        $users = User::all();

        $clients = Client::all();

        return view('calls.edit', compact(['call','users','clients']));
    }

    public function update(CallRequest $request, Call $call)
    {
        $data = $request->validated();

        try {

            $call->update($data);

            return redirect()->route('show.call',$call->id)->withSuccess('Call succesfully updated');

        } catch (\Throwable $th) {

            return redirect()->route('show.call',$call->id)->withErrors('Something went wrong! Try again later..');
        }
    }

    public function destroy(Call $call)
    {
        try {

            $call->delete();

            return redirect()->route('home')->withSuccess('Call succesfully deleted');

        } catch (\Throwable $th) {

            return redirect()->route('home')->withErrors('Something went wrong! Try again later..');
        }

    }

}
