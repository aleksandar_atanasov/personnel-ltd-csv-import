<?php

namespace App\Http\Controllers;

use App\Imports\CsvImport;
use Illuminate\Http\Request;
use App\Http\Requests\ImportRequest;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    public function import(ImportRequest $request)
    {
        $original_path = $request->file('file')->store('imports');
        $path = storage_path('app').'/'.$original_path;

        try {

            Excel::import(new CsvImport, $path);

            return redirect()->back()->withSuccess('Csv file imported succesfully');

        } catch (\Throwable $th) {

           return redirect()->back()->withErrors('Something went wrong, please try again!');
        }
    }
}
