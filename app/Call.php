<?php

namespace App;

use App\User;
use App\Client;
use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
